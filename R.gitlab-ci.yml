spec:
  inputs:
    build-image:
      description: "The image used for buiding/testing the R package"
      default: "${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/rocker/verse:latest-daily"
---

stages:
  - build
  - test
  - deploy

include:
  remote: 'https://gitlab.com/libreumg/ci-templates/raw/main/base.yml'


# https://pat-s.me/using-ccache-to-speed-up-r-package-checks-on-travis-ci/
variables:
  R_REMOTES_NO_ERRORS_FROM_WARNINGS: "TRUE"
  NOT_CRAN: "true"
  R_LIBS_USER: $CI_PROJECT_DIR/Rlib
  CCACHE_DIR: $CI_PROJECT_DIR/ccache
  TZ: Europe/Berlin
  LANG: en_US.UTF-8
  TESTTHAT_CPUS: 8
  FORCE_RELEASE: "false"

.build_image_template: &build_definition
  stage: build
  script:
    - test -e $CI_PROJECT_DIR/Rlib || mkdir $CI_PROJECT_DIR/Rlib
    - bash $SCRIPTS_DIR/install.deps.R.sh
    - Rscript $SCRIPTS_DIR/install.deps.R
    - Rscript $SCRIPTS_DIR/build.R
  artifacts:
    paths:
      - ./*.tar.gz
      - package.tar.gz
  rules:
    - if: '$CI_COMMIT_TAG !~ /^v(\d+[\.\-])*(\d+)$/'

build_image:default:
  image: $[[ inputs.build-image ]]
  cache:
    key: one-cache-to-rule-them-all
    paths:
      - $CI_PROJECT_DIR/Rlib/
      - $CCACHE
    when: always
  <<: *build_definition

build_image:rnold:
  image: struckma/rnold:latest
  cache:
    key: one-cache-to-rule-them-all-rnold
    paths:
      - $CI_PROJECT_DIR/Rlib/
      - $CCACHE
    when: always
  <<: *build_definition
  rules:
    - if: '$VB =~ /^true$/' # TODO: find a trigger for this

.test_image_template: &test_definition
  stage: test
  script:
    - test -e $CI_PROJECT_DIR/Rlib || mkdir $CI_PROJECT_DIR/Rlib
    - bash $SCRIPTS_DIR/install.deps.R.sh
    - Rscript $SCRIPTS_DIR/install.deps.R
    - Rscript $SCRIPTS_DIR/check.R
  coverage: '/Coverage: \d+.\d+%/'
  rules:
    - if: '$CI_COMMIT_TAG !~ /^v(\d+[\.\-])*(\d+)$/'
  artifacts:
    when: always
    paths:
      - ./test_output.zip
      - public/
      - lintr_results.json
      - lintr_badge.svg
      - coverage.RData
      - list_results.RData
    reports:
      junit: [report.xml, test-out.xml]
      codequality: public/lintr_results.json
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml
  rules:
    - if: '$CI_COMMIT_TAG !~ /^v(\d+[\.\-])*(\d+)$/'

test_image:default:
  image: $[[ inputs.build-image ]]
  needs:
    - job: build_image:default
      artifacts: true  # maybe not needed
  cache:
    key: one-cache-to-rule-them-all
    paths:
      - $CI_PROJECT_DIR/Rlib/
      - $CCACHE
    policy: pull
  <<: *test_definition

test_image:rnold:
  image: struckma/rnold:latest
  needs:
    - job: build_image:rnold
      artifacts: true  # maybe not needed
  cache:
    key: one-cache-to-rule-them-all-rnold
    paths:
      - $CI_PROJECT_DIR/Rlib/
      - $CCACHE
    policy: pull
  <<: *test_definition
  rules:
    - if: '$VB =~ /^true$/' # TODO: find a trigger for this

find_todos:
  image: $[[ inputs.build-image ]]
  cache:
    key: one-cache-to-rule-them-all
    paths:
      - $CI_PROJECT_DIR/Rlib/
      - $CCACHE
    policy: pull
  stage: build
  script:
    - Rscript $SCRIPTS_DIR/find_todos.R
  artifacts:
    paths:
      - public
  rules:
    - if: '$CI_COMMIT_TAG !~ /^v(\d+[\.\-])*(\d+)$/'

pages:
  stage: deploy
  script:
    - ls public/
  dependencies:
    - test_image:default
  artifacts:
    paths:
      - public
  only:
    - master
    - main

generate-release:
  image: $[[ inputs.build-image ]]
  stage: build
  script:
      - export VB=$(Rscript $SCRIPTS_DIR/isVersionInc.R)
      - Rscript $SCRIPTS_DIR/create_release_yml.R
  artifacts:
    paths:
      - release.yml
  rules:
    - if: '$CI_COMMIT_TAG !~ /^v(\d+[\.\-])*(\d+)$/'

child-pipeline:
  stage: deploy
  needs:
    - generate-release
    - test_image:default
    - job: build_image:default
      artifacts: true  # maybe not needed
  trigger:
    include:
      - artifact: release.yml
        job: generate-release
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID
  rules:
    - if: '$CI_COMMIT_TAG !~ /^v(\d+[\.\-])*(\d+)$/ && $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH'

# TODO: add at least here: - $CI_COMMIT_MESSAGE =~ /^DRAFT:/ option to suppress parts of the pipeline, better in all templates, see dataquieR
